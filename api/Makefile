.PHONY: clean critic security lint test build run

APP_NAME = apiserver
BUILD_DIR = $(PWD)/build

clean:
	rm -rf ./build

critic:
	gocritic check -enableAll ./...

security:
	gosec ./...

lint:
	golangci-lint run ./... --timeout=5m

test: clean critic security lint
	go test -v -timeout 30s -race -tags unitgo -covermode=atomic -coverprofile cover.out -coverpkg=./... ./...
	go tool cover -func=cover.out

build: test
	CGO_ENABLED=0 go build -ldflags="-w -s" -o $(BUILD_DIR)/$(APP_NAME) cmd/main.go

build.dev:
	CGO_ENABLED=0 go build -ldflags="-w -s" -o $(BUILD_DIR)/$(APP_NAME) cmd/main.go

run: build
	$(BUILD_DIR)/$(APP_NAME)
	
docker.apiserver.build:
	docker build -t apiserver -f Dockerfile.prod .
	
docker.run: docker.network docker.postgres docker.apiserver

docker.network:
	docker network inspect dev-network >/dev/null 2>&1 || \
	docker network create -d bridge dev-network

docker.apiserver: docker.apiserver.build
	docker run --rm -d \
		--name hello-world \
		--network dev-network \
		-p 3000:3000 \
		-e DB_USER=postgres \
		-e DB_PASSWORD=password \
		-e DB_HOST=db \
		-e DB_NAME=postgres \
		apiserver

docker.postgres:
	docker run --rm -d \
		--name db \
		--network dev-network \
		-e POSTGRES_USER=postgres \
		-e POSTGRES_PASSWORD=password \
		-e POSTGRES_DB=postgres \
		-v ${HOME}/dev-postgres/data/:/var/lib/postgresql/data \
		-p 5432:5432 \
		postgres

docker.stop: docker.stop.apiserver docker.stop.postgres

docker.stop.apiserver:
	docker stop hello-world

docker.stop.postgres:
	docker stop db
