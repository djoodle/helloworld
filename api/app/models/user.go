package models

import (
	"gorm.io/gorm"
	"time"
)

type User struct {
	gorm.Model
	Username    string    `json:"username" gorm:"primaryKey;text;not null;default:null"`
	DateOfBirth time.Time `json:"dateOfBirth" gorm:"not null"`
}
