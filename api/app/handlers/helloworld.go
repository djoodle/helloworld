package handlers

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/djoodle/helloworld/api/app/models"
	"gitlab.com/djoodle/helloworld/api/pkg/configs"
	"time"
)

var validate = validator.New()
var Now = time.Now

type (
	helloReq struct {
		Username string `validate:"required,alpha"`
	}

	helloPutReq struct {
		helloReq
		DateOfBirth string `validate:"required"`
	}
)

func PutHelloHandler(cfg configs.AppConfig) fiber.Handler {
	db := cfg.DB()
	log := cfg.Log().With().Str("handler", "put-hello-handler").Logger()

	return func(c *fiber.Ctx) error {
		log := log.With().Str("id", c.Get(fiber.HeaderXRequestID)).Logger()
		req := &helloPutReq{helloReq: helloReq{Username: c.Params("username")}}
		if err := c.BodyParser(req); err != nil {
			log.Warn().Err(err).Msg("could not unmarshal the request body")
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
				"error": err.Error(),
			})
		}

		if err := validate.Struct(req); err != nil {
			log.Warn().Err(err).Msg("cannot validate request")
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
				"error": err.Error(),
			})
		}

		// convert dob and validate
		dob, err := time.Parse("2006-01-02", req.DateOfBirth)
		if err != nil {
			log.Warn().Err(err).Msg("unparseable d.o.b")
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
				"error": err.Error(),
			})

		}

		// dob must be in past
		if dob.After(Now().Truncate(24 * time.Hour).Add(-1 * time.Nanosecond)) {
			log.Warn().Msg("date of birth must be in the past")
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
				"error": "must be a date before the today date.",
			})
		}

		if err := db.Where(models.User{Username: req.Username}).
			Assign(models.User{Username: req.Username, DateOfBirth: dob}).
			FirstOrCreate(&models.User{}).Error; err != nil {
			log.Error().Err(err).Msg("db update failed")
			return err
		}

		return c.Status(204).Send(nil)
	}
}

func GetHelloHandler(cfg configs.AppConfig) fiber.Handler {
	db := cfg.DB()
	log := cfg.Log().With().Str("handler", "get-hello-handler").Logger()

	return func(c *fiber.Ctx) error {
		log := log.With().Str("id", c.Get(fiber.HeaderXRequestID)).Logger()
		req := &helloReq{
			Username: c.Params("username"),
		}

		if err := validate.Struct(req); err != nil {
			log.Warn().Err(err).Msg("username is invalid")
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
				"error": err.Error(),
			})
		}

		user := models.User{Username: req.Username}
		db.First(&user)

		if user.DateOfBirth.IsZero() {
			log.Warn().Msg("no user found in DB")
			return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
				"error": "no user with that name in the database",
			})
		}

		msg := fmt.Sprintf("Hello, %s!", user.Username)
		days := daysUntilBday(user.DateOfBirth)
		if days == 0 {
			msg += " Happy birthday!"
		} else {
			msg = fmt.Sprintf("%s Your birthday is in %d day(s)", msg, days)
		}

		return c.JSON(fiber.Map{
			"message": msg,
		})
	}
}

func daysUntilBday(birthday time.Time) int {
	var nextBirthday = birthday.AddDate(Now().Year()-birthday.Year(), 0, 0)
	if nextBirthday.Before(Now().Truncate(time.Hour * 24)) {
		nextBirthday = nextBirthday.AddDate(1, 0, 0)
	}
	return int((nextBirthday.Sub(Now().Truncate(24 * time.Hour)).Hours()) / 24)
}
