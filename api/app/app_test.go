package app_test

import (
	"database/sql"
	"database/sql/driver"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/djoodle/helloworld/api/app/handlers"
	"gitlab.com/djoodle/helloworld/api/pkg/configs"
	"gitlab.com/djoodle/helloworld/api/pkg/routes"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"io"
	"net/http/httptest"
	"regexp"
	"strings"
	"testing"
	"time"
)

type AnyTime struct{}

// Match satisfies sqlmock.Argument interface
func (a AnyTime) Match(v driver.Value) bool {
	_, ok := v.(time.Time)
	return ok
}

func TestApp(t *testing.T) {
	// test setup
	stubCfg := configs.New()
	var (
		db   *sql.DB
		err  error
		mock sqlmock.Sqlmock
	)

	db, mock, err = sqlmock.New()
	if err != nil {
		t.Errorf("Failed to open mock sql db, got error: %v", err)
	}
	if db == nil {
		t.Error("mock db is null")
	}
	if mock == nil {
		t.Error("sqlmock is null")
	}

	dialector := postgres.New(postgres.Config{
		DSN:                  "sqlmock_db_0",
		DriverName:           "postgres?parseTime=true",
		Conn:                 db,
		PreferSimpleProtocol: true,
	})
	gormDb, err := gorm.Open(dialector, &gorm.Config{})
	if err != nil {
		t.Errorf("Failed to open gorm v2 db, got error: %v", err)
	}

	if gormDb == nil {
		t.Error("gorm db is null")
	}

	defer db.Close()

	stubCfg.WithDB(gormDb)
	routes.Routes(stubCfg.App(), stubCfg)

	// hack to fix time
	handlers.Now = func() time.Time { return time.Date(2023, 4, 12, 6, 30, 0, 0, time.UTC) }

	// Define a structure for specifying input and output data
	// of a single test case
	tests := []struct {
		description  string // description of the test case
		route        string // route path to test
		expectedCode int    // expected HTTP status codei
		method       string
		response     string
		body         string
		mockClosure  func(mock sqlmock.Sqlmock)
	}{
		//
		// GET /api/v1/hello
		//
		{
			description:  "get existing user with birthday today return HTTP status 200",
			route:        "/api/v1/hello/dudell",
			expectedCode: 200,
			method:       "GET",
			response:     `{"message":"Hello, dudell! Happy birthday!"}`,
			mockClosure: func(mock sqlmock.Sqlmock) {
				var (
					uname = "dudell"
					dob   = time.Date(2001, 4, 12, 0, 0, 0, 0, time.UTC)
				)
				mock.ExpectQuery(
					`SELECT \* FROM "users" WHERE "users"\."deleted_at" IS NULL AND "users"\."username" = \$1 ORDER BY "users"\."id" LIMIT 1`).
					WithArgs(uname).
					WillReturnRows(sqlmock.NewRows([]string{"username", "date_of_birth"}).
						AddRow(uname, dob))
			},
		},
		{
			description:  "get existing user with birthday later in the year return HTTP status 200",
			route:        "/api/v1/hello/dudell",
			expectedCode: 200,
			method:       "GET",
			response:     `Hello, dudell! Your birthday is in 172 day(s)`,
			mockClosure: func(mock sqlmock.Sqlmock) {
				var (
					uname = "dudell"
					dob   = time.Date(1989, 10, 1, 0, 0, 0, 0, time.UTC)
				)
				mock.ExpectQuery(
					`SELECT \* FROM "users" WHERE "users"\."deleted_at" IS NULL AND "users"\."username" = \$1 ORDER BY "users"\."id" LIMIT 1`).
					WithArgs(uname).
					WillReturnRows(sqlmock.NewRows([]string{"username", "date_of_birth"}).
						AddRow(uname, dob))
			},
		},
		{
			description:  "get existing user with birthday earlier in the year return HTTP status 200",
			route:        "/api/v1/hello/dudell",
			expectedCode: 200,
			method:       "GET",
			response:     `Hello, dudell! Your birthday is in 264 day(s)`,
			mockClosure: func(mock sqlmock.Sqlmock) {
				var (
					uname = "dudell"
					dob   = time.Date(2004, 1, 1, 0, 0, 0, 0, time.UTC)
				)
				mock.ExpectQuery(
					`SELECT \* FROM "users" WHERE "users"\."deleted_at" IS NULL AND "users"\."username" = \$1 ORDER BY "users"\."id" LIMIT 1`).
					WithArgs(uname).
					WillReturnRows(sqlmock.NewRows([]string{"username", "date_of_birth"}).
						AddRow(uname, dob))
			},
		},
		{
			description:  "get user that doesn't exist returns a 404",
			route:        "/api/v1/hello/dudell",
			expectedCode: 404,
			method:       "GET",
			response:     `no user with that name in the database`,
			mockClosure: func(mock sqlmock.Sqlmock) {
				var (
					uname = "dudell"
				)
				mock.ExpectQuery(
					`SELECT \* FROM "users" WHERE "users"\."deleted_at" IS NULL AND "users"\."username" = \$1 ORDER BY "users"\."id" LIMIT 1`).
					WithArgs(uname).
					WillReturnRows(sqlmock.NewRows([]string{"username", "date_of_birth"}))
			},
		},
		{
			description:  "get HTTP status 404, when route is not exists",
			route:        "/not-found",
			expectedCode: 404,
			method:       "GET",
			response:     `{"error":"Cannot GET /not-found"}`,
		},
		{
			description:  "get with a null username returns 400",
			route:        "/api/v1/hello/",
			expectedCode: 404,
			method:       "GET",
			response:     `Cannot GET /api/v1/hello`,
		},
		{
			description:  "username can't contain numbers",
			route:        "/api/v1/hello/bob123",
			expectedCode: 400,
			method:       "GET",
			response:     `validation for 'Username' failed`,
		},
		{
			description:  "username can't contain symbols",
			route:        "/api/v1/hello/bob@#$",
			expectedCode: 400,
			method:       "GET",
			response:     `validation for 'Username' failed`,
		},
		//
		// PUT /api/v1/hello
		//
		{
			description:  "get with a null username returns 400",
			route:        "/api/v1/hello/",
			expectedCode: 404,
			method:       "PUT",
			response:     `Cannot PUT /api/v1/hello`,
		},
		{
			description:  "username can't contain numbers",
			route:        "/api/v1/hello/bob123",
			expectedCode: 400,
			method:       "PUT",
			body:         "{}",
			response:     `validation for 'Username' failed`,
		},
		{
			description:  "username can't contain symbols",
			route:        "/api/v1/hello/bob@#$",
			expectedCode: 400,
			method:       "PUT",
			body:         "{}",
			response:     `validation for 'Username' failed`,
		},
		{
			description:  "dateOfBirth is required",
			route:        "/api/v1/hello/bob",
			expectedCode: 400,
			method:       "PUT",
			body:         "{}",
			response:     `'DateOfBirth' failed on the 'required'`,
		},
		{
			description:  "dateOfBirth must be a date",
			route:        "/api/v1/hello/bob",
			expectedCode: 400,
			method:       "PUT",
			body:         `{"dateOfBirth": "not_a_date"}`,
			response:     `cannot parse`,
		},
		{
			description:  "dateOfBirth must be in the past",
			route:        "/api/v1/hello/bob",
			expectedCode: 400,
			method:       "PUT",
			body:         `{"dateOfBirth": "2025-01-01"}`,
			response:     `must be a date before the today date`,
		},
		{
			description:  "dateOfBirth cannot be today",
			route:        "/api/v1/hello/bob",
			expectedCode: 400,
			method:       "PUT",
			body:         `{"dateOfBirth": "2023-04-12"}`,
			response:     `must be a date before the today date`,
		},
		{
			description:  "new user is added",
			route:        "/api/v1/hello/bob",
			expectedCode: 204,
			method:       "PUT",
			body:         `{"dateOfBirth": "2005-04-12"}`,
			mockClosure: func(mock sqlmock.Sqlmock) {
				var (
					uname = "bob"
					dob   = time.Date(2005, 4, 12, 0, 0, 0, 0, time.UTC)
				)
				mock.ExpectQuery(regexp.QuoteMeta(
					`SELECT * FROM "users" WHERE "users"."username" = $1 AND "users"."deleted_at" IS NULL ORDER BY "users"."id" LIMIT 1`,
				)).
					WithArgs(uname).
					WillReturnRows(sqlmock.NewRows([]string{"username", "date_of_birth"}))
				mock.ExpectBegin()
				mock.ExpectQuery(regexp.QuoteMeta(
					`INSERT INTO "users" ("created_at","updated_at","deleted_at","date_of_birth","username") VALUES ($1,$2,$3,$4,$5) RETURNING "id","username"`,
				)).
					WithArgs(AnyTime{}, AnyTime{}, nil, dob, uname).
					WillReturnRows(sqlmock.NewRows([]string{"username", "date_of_birth"}).
						AddRow(uname, dob))
				mock.ExpectCommit()
			},
		},
		{
			description:  "dateOfBirth is updated",
			route:        "/api/v1/hello/bob",
			expectedCode: 204,
			method:       "PUT",
			body:         `{"dateOfBirth": "2005-04-12"}`,
			mockClosure: func(mock sqlmock.Sqlmock) {
				var (
					uname = "bob"
					dob   = time.Date(2003, 4, 12, 0, 0, 0, 0, time.UTC)
				)
				mock.ExpectQuery(
					`SELECT \* FROM "users" WHERE "users"\."username" = \$1 AND "users"\."deleted_at" IS NULL ORDER BY "users"\."id" LIMIT 1`).
					WithArgs(uname).
					WillReturnRows(sqlmock.NewRows([]string{"username", "date_of_birth"}).AddRow(uname, dob))
				mock.ExpectBegin()
				mock.ExpectExec(regexp.QuoteMeta(
					`UPDATE "users" SET "date_of_birth"=$1,"username"=$2,"updated_at"=$3 WHERE "users"."username" = $4 AND "users"."deleted_at" IS NULL AND "username" = $5`,
				)).
					WithArgs(time.Date(2005, 4, 12, 0, 0, 0, 0, time.UTC), uname, AnyTime{}, uname, uname).
					WillReturnResult(sqlmock.NewResult(1, 1))
				mock.ExpectCommit()
			},
		},
	}
	// Iterate through test single test cases
	for _, test := range tests {
		// Create a new http request with the route from the test case
		req := httptest.NewRequest(test.method, test.route, strings.NewReader(test.body))
		req.Header.Set("Content-Type", "application/json")

		// set mock expectations
		if test.mockClosure != nil {
			test.mockClosure(mock)
		}

		// Perform the request plain with the app,
		// the second argument is a request latency
		// (set to -1 for no latency)
		resp, _ := stubCfg.App().Test(req)
		// Verify, if the status code is as expected
		assert.Equalf(t, test.expectedCode, resp.StatusCode, test.description)
		b, err := io.ReadAll(resp.Body)
		// b, err := ioutil.ReadAll(resp.Body)  Go.1.15 and earlier
		if err != nil {
			t.Fatal(err)
		}
		resp.Body.Close()
		assert.Contains(t, string(b), test.response)

		// Checking all expectations were met
		if test.mockClosure != nil {
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("there were unfulfilled expectations: %s", err)
			}
		}
	}
}
