package main

import (
	"os"

	"gitlab.com/djoodle/helloworld/api/pkg/configs"
	"gitlab.com/djoodle/helloworld/api/pkg/middleware"
	"gitlab.com/djoodle/helloworld/api/pkg/routes"
	"gitlab.com/djoodle/helloworld/api/pkg/server"

	_ "github.com/joho/godotenv/autoload" // load .env file automatically
)

func main() {
	// Define Fiber config.
	cfg := configs.New()
	app := cfg.App()
	// initialize the DB cnx on startup
	_ = cfg.DB()

	// Middlewares.
	middleware.FiberMiddleware(app, cfg) // Register Fiber's middleware for app.

	// Routes.
	routes.Routes(app, cfg)

	// Start server (with or without graceful shutdown).
	if os.Getenv("STAGE_STATUS") == "dev" {
		server.Start(app)
	} else {
		server.StartWithGracefulShutdown(app)
	}
}
