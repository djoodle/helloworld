package configs

import (
	"errors"
	"os"
	"strings"
	"sync"

	"gitlab.com/djoodle/helloworld/api/pkg/database"

	"github.com/ansrivas/fiberprometheus/v2"
	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog"
	"gorm.io/gorm"
)

type (
	PromMiddlewhereProvider interface {
		Middleware(ctx *fiber.Ctx) error
		RegisterAt(app fiber.Router, url string, handlers ...fiber.Handler)
	}

	Database interface {
		Where(query interface{}, args ...interface{}) *gorm.DB
		First(dest interface{}, conds ...interface{}) *gorm.DB
		FirstOrCreate(dest interface{}, conds ...interface{}) *gorm.DB
		Assign(attrs ...interface{}) *gorm.DB
	}

	AppConfig interface {
		DB() Database
		Log() *zerolog.Logger
		Prom() PromMiddlewhereProvider
		App() *fiber.App

		WithDB(Database)
		WithProm(PromMiddlewhereProvider)
		WithLog(*zerolog.Logger)
	}

	config struct {
		db   Database
		log  *zerolog.Logger
		prom PromMiddlewhereProvider
		app  *fiber.App

		lo  sync.Once
		dbo sync.Once
		po  sync.Once
	}
)

// New returns a bootstrapped application configuration - including a DB connection
// and an instantiated fiber app.
func New() AppConfig {
	cfg := &config{}

	// create the base app
	cfg.app = fiber.New(fiber.Config{
		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			// Status code defaults to 500
			code := fiber.StatusInternalServerError

			// Retrieve the custom status code if it's a *fiber.Error
			var e *fiber.Error
			if errors.As(err, &e) {
				code = e.Code
			}

			// Send custom error page
			err = ctx.Status(code).JSON(fiber.Map{
				"error": err.Error(),
			})
			if err != nil {
				// In case the SendFile fails
				return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
					"error": "Internal Server Error",
				})
			}

			// Return from handler
			return nil
		},
	})

	return cfg
}

// NewStub returns a blank application configuration for mocking in tests
func NewStub() AppConfig {
	return &config{}
}

// App is an accessor to the underlying fiber app
func (cfg *config) App() *fiber.App {
	return cfg.app
}

// Log allows us to access the singleton logger
func (cfg *config) Log() *zerolog.Logger {
	cfg.lo.Do(func() {
		if cfg.log == nil {
			lvlString := os.Getenv("LOG_LEVEL")
			if lvlString == "" {
				lvlString = "info"
			}

			zerolog.SetGlobalLevel(logLevels[strings.ToLower(lvlString)])
			logger := zerolog.New(os.Stdout)
			cfg.log = &logger
		}
	})
	return cfg.log
}

// DB allows us to access the single DB connection
func (cfg *config) DB() Database {
	cfg.dbo.Do(func() {
		if cfg.db == nil {
			var err error
			cfg.db, err = database.ConnectDb(&database.Config{
				Host:     os.Getenv("DB_HOST"),
				User:     os.Getenv("DB_USER"),
				Password: os.Getenv("DB_PASSWORD"),
				Name:     os.Getenv("DB_NAME"),
				Logger:   *cfg.Log(),
			})

			if err != nil {
				panic(err)
			}
		}
	})
	return cfg.db
}

// Prom returns a middle ware provider for metrics
func (cfg *config) Prom() PromMiddlewhereProvider {
	cfg.po.Do(func() {
		if cfg.prom == nil {
			cfg.prom = fiberprometheus.New("helloworld")
		}
	})
	return cfg.prom
}

// With functions help us bootstrap our testing version

func (cfg *config) WithDB(db Database) {
	cfg.db = db
}

func (cfg *config) WithLog(log *zerolog.Logger) {
	cfg.log = log
}

func (cfg *config) WithProm(prom PromMiddlewhereProvider) {
	cfg.prom = prom
}

var logLevels = map[string]zerolog.Level{
	zerolog.DebugLevel.String(): zerolog.DebugLevel,
	zerolog.InfoLevel.String():  zerolog.InfoLevel,
	zerolog.WarnLevel.String():  zerolog.WarnLevel,
	zerolog.ErrorLevel.String(): zerolog.ErrorLevel,
}

var _ AppConfig = (*config)(nil)
