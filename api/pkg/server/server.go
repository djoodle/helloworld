package server

import (
	"fmt"
	"os"
	"os/signal"

	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"
)

var (
	cnxString = fmt.Sprintf(
		"%s:%s",
		os.Getenv("SERVER_HOST"),
		os.Getenv("SERVER_PORT"),
	)
)

// StartWithGracefulShutdown function for starting server with a graceful shutdown.
func StartWithGracefulShutdown(a *fiber.App) {
	// Create channel for idle connections.
	idleConnsClosed := make(chan struct{})

	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt) // Catch OS signals.
		<-sigint

		// Received an interrupt signal, shutdown.
		if err := a.Shutdown(); err != nil {
			// Error from closing listeners, or context timeout:
			log.Error().Err(err).Msg("unable to shutdown server gracefully")
		}

		close(idleConnsClosed)
	}()

	// Run server.
	if err := a.Listen(cnxString); err != nil {
		log.Error().Err(err).Msg("server is not running")
	}

	<-idleConnsClosed
}

// Start func for starting a simple server.
func Start(a *fiber.App) {
	// Run server.
	if err := a.Listen(cnxString); err != nil {
		log.Error().Err(err).Msg("server is not running")
	}
}
