package middleware

// This is largely modified from https://github.com/dre1080/fiberlog which
// only works for v1 of fiber. The main change is explicitly calling
// the errorhandler on a failure.

import (
	"fmt"
	"net/http"
	"os"
	"runtime/debug"
	"sync"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// LogCfg is the configuration for the Log middlewherer
type LogCfg struct {
	// Next defines a function to skip this middleware.
	Next func(ctx *fiber.Ctx) bool

	// Logger is a *zerolog.Logger that writes the logs.
	//
	// Default: log.Logger.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	Logger *zerolog.Logger
}

// NewLog creates a new logging middleware that user the zerolog logger
// and includes key fields such as path, status and request ID
func NewLog(config ...LogCfg) fiber.Handler {
	var conf LogCfg
	if len(config) > 0 {
		conf = config[0]
	}

	var sublog zerolog.Logger
	if conf.Logger == nil {
		sublog = log.Logger.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	} else {
		sublog = *conf.Logger
	}

	var (
		once       sync.Once
		errHandler fiber.ErrorHandler
	)

	return func(c *fiber.Ctx) error {
		// Don't execute the middleware if Next returns true
		if conf.Next != nil && conf.Next(c) {
			return c.Next()
		}

		// Set error handler once
		once.Do(func() {
			// override error handler
			errHandler = c.App().ErrorHandler
		})

		start := time.Now()

		rid := c.Get(fiber.HeaderXRequestID)
		if rid == "" {
			rid = uuid.New().String()
			c.Set(fiber.HeaderXRequestID, rid)
		}

		f := &fields{
			ID:       rid,
			RemoteIP: c.IP(),
			Method:   c.Method(),
			Host:     c.Hostname(),
			Path:     c.Path(),
			Protocol: c.Protocol(),
		}

		defer func() {
			rvr := recover()

			if rvr != nil {
				err, ok := rvr.(error)
				if !ok {
					err = fmt.Errorf("%v", rvr)
				}

				f.Error = err
				f.Stack = debug.Stack()

				c.Status(http.StatusInternalServerError)
				_ = c.JSON(fiber.Map{
					"error": "panic handling request",
				})
			}

			f.StatusCode = c.Response().StatusCode()
			f.Latency = time.Since(start).Seconds()

			switch {
			case rvr != nil:
				sublog.Error().EmbedObject(f).Msg("panic recover")
			case f.StatusCode >= 500:
				sublog.Error().EmbedObject(f).Msg("server error")
			case f.StatusCode >= 400:
				sublog.Warn().EmbedObject(f).Msg("client error")
			case f.StatusCode >= 300:
				sublog.Warn().EmbedObject(f).Msg("redirect")
			case f.StatusCode >= 200:
				sublog.Info().EmbedObject(f).Msg("success")
			case f.StatusCode >= 100:
				sublog.Info().EmbedObject(f).Msg("informative")
			default:
				sublog.Warn().EmbedObject(f).Msg("unknown status")
			}
		}()
		// handle request
		nxtErr := c.Next()
		// Manually call error handler
		if nxtErr != nil {
			return errHandler(c, nxtErr)
		}
		return nil
	}
}

type fields struct {
	ID         string
	RemoteIP   string
	Host       string
	Method     string
	Path       string
	Protocol   string
	StatusCode int
	Latency    float64
	Error      error
	Stack      []byte
}

func (f *fields) MarshalZerologObject(e *zerolog.Event) {
	e.
		Str("id", f.ID).
		Str("remote_ip", f.RemoteIP).
		Str("host", f.Host).
		Str("method", f.Method).
		Str("path", f.Path).
		Str("protocol", f.Protocol).
		Int("status_code", f.StatusCode).
		Float64("latency", f.Latency).
		Str("tag", "request")

	if f.Error != nil {
		e.Err(f.Error)
	}

	if f.Stack != nil {
		e.Bytes("stack", f.Stack)
	}
}
