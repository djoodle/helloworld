package middleware

import (
	"gitlab.com/djoodle/helloworld/api/pkg/configs"

	healthcheck "github.com/aschenmaker/fiber-health-check"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/pprof"
)

// FiberMiddleware enables a number of built in, third part or custom middlewares.
// See: https://docs.gofiber.io/api/middleware
func FiberMiddleware(a *fiber.App, cfg configs.AppConfig) {
	prom := cfg.Prom()
	prom.RegisterAt(a, "/metrics")
	a.Use(
		// Log middleware
		NewLog(LogCfg{
			Logger: cfg.Log(),
			Next: func(ctx *fiber.Ctx) bool {
				return ctx.Get("X-Health-Check") == "1"
			},
		}),
		// Profiling middleware
		pprof.New(),
		// Prom Metrics middleware
		prom.Middleware,
		// Add CORS,
		cors.New(),
		// Healthcheck middleware
		healthcheck.New(),
	)
}
