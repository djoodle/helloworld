package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/djoodle/helloworld/api/app/handlers"
	"gitlab.com/djoodle/helloworld/api/pkg/configs"
)

// Routes configures the handlers on the write endpoints
func Routes(a *fiber.App, cfg configs.AppConfig) {
	// Create routes group.
	route := a.Group("/api/v1")

	// Assign the handlers
	route.Get("/hello/:username", handlers.GetHelloHandler(cfg))
	route.Put("/hello/:username", handlers.PutHelloHandler(cfg))
}
