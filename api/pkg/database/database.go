package database

import (
	"fmt"

	"github.com/rs/zerolog"
	"github.com/wei840222/gorm-zerolog"
	"gitlab.com/djoodle/helloworld/api/app/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// Config describes the database config
type Config struct {
	User, Password, Name, Host string
	Logger                     zerolog.Logger
}

// ConnectDb will create a connection to postgres and migrate the db schema
func ConnectDb(cfg *Config) (*gorm.DB, error) {
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=5432 sslmode=disable TimeZone=Asia/Shanghai",
		cfg.Host, cfg.User, cfg.Password, cfg.Name,
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: gorm_zerolog.NewWithLogger(cfg.Logger),
	})

	if err != nil {
		cfg.Logger.Error().Err(err).Msg("failed to connect to database")
		return nil, err
	}

	cfg.Logger.Info().Msg("database connected")
	if err := db.AutoMigrate(&models.User{}); err != nil {
		cfg.Logger.Error().Err(err).Msg("database migrations failed")
		return nil, err
	}
	cfg.Logger.Info().Msg("database migrations completed")

	return db, nil
}
