# Helloworld DevOps Excercise

![server diagram](diagram.png "Deployment Architecture")

## App
The app is written in go. Without over indexing on writing a production ready app
some considerations have been made to:

- logging: Using structured logs with request ID injected into logger
- metrics: Using promql middleware to expose http metrics
- profilign: Enabled pprof middleware so can be perf tuned.

The app is built with the fiber go web framework, and uses postgres to store data. No cache
currently.

### Localdev
The easiest way to run the application is via docker-compose from the parentl directory. This
will start the application with live reload, annd the database. 

```
  docker-compose up
```
  
There are make targets for linting, testing, building and containerisation of the binary. You can
also run up the prod container (with postgres) using:

```
make docker.run
make docker.stop
```

This is to help test the prod build.

## CI

Used gitlab-ci to test and build the app. I've gone for "getting stuff done" over locking it down 
(anyone can push to main atm) - however the runner config shows examples of how to move towards
CI/CD pipelines with this stack.

## GCloud Deployment
I've deployed the platform onto my own google cloud account - chosing GKE as the platform. The GKE cluster
is provisioned via [pulumi](https://www.pulumi.com). 

The postgres stateful set was taken from readily avaialble communtiy configurations, with k8s yaml
produced for the specific webapp.